
# Démos d'une architecture d'application pour les étudiants.
Auteur: Philippe Alluin p.alluin@phaln.info

Elle utilise une version simple d'une entité et d'un repository, en utilisant la
 librairie `Phaln`.

## Les dossiers importants:

Le dossier `/config` contient les fichiers de configuration:
- `appConfig.local.php` est le modèle du fichier de configuration de l'application.
  Copiez ce fichier puis renommez-le en enlevant le `.local` dans son nom.
  Réglez les paramètres:
    - `$infoBdd` pour connexion à la bdd. 
    - La définition de `URL_BASE`, qui est l'url de base de votre application.
    - La définition d'une constante `DUMP` utile pour activer/désactiver les var_dump, 
à condition d'utiliser la notation:
    `if(DUMP) var_dump($maVar);` ou `dump_var($var, DUMP, 'Un message présentant $var');`

  Les sessions sont démarrées après l'inclusion de `globalConfig.php`. Cela permet 
d'avoir l'autoload défini et pouvoir mettre des objets en session.
  
  **ATTENTION!! Ce fichier `appConfig.php` est à inclure en début de toutes vos 
pages. !!ATTENTION**

- `globalConfig.php` contient une configuration générale, lrinclusion de l'autoload 
de composer pour l'utilisation des classes, les constantes avec les noms des 
dossiers pour la librairie, etc. Il ne devrait pas être modifié.

Le dossier `/public` contient les pages publiques de l'application et les assets: 
css, images, javascript. Normalement, en mvc, le fichier `/public/index.php` 
est la page principale de l'application... et même potentiellement la seule!

Le dossier `/src` est la dossier de base pour vos développement PHP. Il contient
 les espaces de nom comme `Entities` (attention à la majuscule...).

Le fichier `/index.php` renvoit vers le fichier `/public/index.php` qui est la
 page d'accueil de l'application.

## Installation:
- ouvrez un terminal dans le dossier `www`
- clonez le projet `skeleton-light` (identification sans doute demandée) vers le
 dossier de votre choix (`MyProj` par exemple):

`git clone https://gitlab.com/phalnprojects/skeleton-light.git MyProj`

- Rendez-vous dans le dossier `MyProj`:

`cd MyProj`

- Installer les librairies (dont `Phaln`) dans le dossier `vendor` à l'aide de
 composer (utilise le fichier `composer.json`):

`composer install`

- Si vous souhaitez utiliser NPM pour installer vos assets (css, js, ...) 
regardez le fichier `package.json` en exemple qui installe `BootStrap 4` et les
 librairies nécessaires et les icones `fontawesome`. Pour l'utiliser:

`npm install`
